{ nixpkgs,... }@extraArgs:
let
wb = builtins.concatLists (builtins.genList (
            x: let
              ws = let
                c = (x + 1) / 10;
              in
                builtins.toString (x + 1 - (c * 10));
            in [
              "$mod, ${ws}, workspace, ${toString (x + 1)}"
              "$mod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
            ]
          )
          10);
 
in
{
  nixpkgs.config.allowUnfreePredicate = _:  true;
  
  wayland.windowManager.hyprland = {
  enable = true;
  settings = {
    "$mod" = "SUPER";
    "$terminal" = "alacritty";
    binde = [
      "$mod, T, exec, $terminal"
      "$mod,D,exec, wofi --show drun"
      "$mod,K,killactive"
      ", XF86AudioMute, exec, wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle"
      "$mod, O,submap,open"
      ",XF86AudioRaiseVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 0.1+"
      ",XF86AudioLowerVolume, exec, wpctl set-volume @DEFAULT_AUDIO_SINK@ 0.1-"
      
    ] ++ wb;
    exec-once= [
      "waybar"
      "mako"

    ];

         
  };
  extraConfig = ''
    submap=open
    binde=,b,exec, brave
    binde=,l,exec, libreoffice

    binde=,escape,submap, reset
    submap=reset
  '';
};

  wayland.windowManager.sway = {
    enable = true;
    config = {
    };
  
  };
  programs = {
    carapace = {
      enable = true;
    };
    waybar = { 
      enable = true;
      settings = {
        mainbar = {
          layer = "top";
          position = "bottom";
          modules-right = ["battery" "disk" "pulseaudio"];
          modules-left = [ "wlr/taskbar" "hyprland/workspaces"];
          modules-center = ["hyprland/submap" ];
       
    }; 
  };
};

    tmux = {
      enable = true;
    };
    emacs = {
      enable = true;
      extraPackages = (epkgs: with epkgs; [nix-mode evil use-package exwm lsp-mode ivy pdf-tools]);
      extraConfig = "
      (load-theme 'wombat)
      (evil-mode t)
      (package-use 'ivy)
      (package-use 'pdf-tools)
      (tool-bar-mode nil)
      (supress-startup-message t)
      ";
    };

	  gpg = {

  		enable=true;
    };



    nushell = {
      enable = true;
    };
	  neovim = {
		  enable = true;
		  plugins = with nixpkgs.vimPlugins; [
                    vim-nix
                    nvim-lspconfig
                       ];
                       extraConfig = ''
                        lua  require"lspconfig".nixd.setup{}
                         '';
	  };
          zoxide = {
            enable = true;
            enableZshIntegration = true;
    };
    zsh = {
		  enable = true;
      syntaxHighlighting.enable = true;
      autosuggestion.enable = true;
	    autocd = true;
	    dotDir = ".config/zsh";



	  };


    brave = {
	    enable = true;
	    extensions = [{
	      id = "oboonakemofpalcgghocfoadofidjkkk";


	    }
	    {
	      id="dbepggeogbaibhgnhhndojpepiihcmeb";



	    }

	    ];


    };

  };
  home.sessionVariables = {
	  EDITOR="nvim";


  };
  home = {
    packages = with nixpkgs; [
      discord
      bleachbit
      yt-dlp
      mpv
      dmidecode
      minetest
      gzdoom
      keepassxc
      wine64Packages.full
      kdePackages.kdialog
      jq
      qemu
      winetricks
      distrobox
      podman
      distrobox
      evtest
      alacritty
      wofi
      mako
      nixd
      fzf
      pass
    ];
    file.".config/nushell/utils.nu".text = ''

      def "discordcli get" [--pass token api_path --cdn --api-version:int=10 --headers:record = {}]  { 
      let api = if not  $cdn { $"https://discord.com/api/v($api_version)"} else "https://cdn.discordapp.com"; let _token  = if $pass {(pass $token) }  else $token;   http get $"($api)($api_path)" --headers ({Authorization:$_token} | merge $headers )}
      
      def "discordcli user-guilds" [--pass token]  {discordcli get --pass=$pass $token "/users/@me/guilds"}
      def "discordcli yoink-emojis" [                                                                     
 from                                            
  --pass                                         
  token                                          
  save_to:path                                   
 ] {                                             
 mkdir -v $"($save_to)/($from)";                              
alias "discordcli get" = discordcli get  --pass=$pass $token;                  
alias "discordcli user-guilds" = discordcli user-guilds --pass=$pass  $token   
 discordcli user-guilds   |                                                         
  where name == $from |   discordcli get   /guilds/($in.id | to text )/emojis |  
 each {|e| discordcli get --cdn  /emojis/($e.id)        
  | save -fp   $"($save_to)/($from)/($e.name).png"}  
 }   


    '';

    homeDirectory = if extraArgs ? homeDirectory then extraArgs.homeDirectory else /home/nagito;
    username = if extraArgs ? username then extraArgs.username else "nagito";

  };






  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.stateVersion = "21.11";
  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
}
