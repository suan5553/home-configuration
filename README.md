# home manager flake
### a flake mainly containing my home manager config as well as a few packages in the future


### how to use 

```bash
git clone https://gitlab.com/suan5553/home-configuration
cd home-configuration
home-manager switch --flake  .#nagito
```
you can change the username & homeDirectory of the user
by passing it via extraSpecialArgs to the module 
[flake.nix](flake.nix)

