{
  description = "my home manager flake";

  inputs = {
   
    # Specify the source of Home Manager and Nixpkgs.
    nixpkgs.url = "github:nixos/nixpkgs/";
   flake-compat.url = "https://flakehub.com/f/edolstra/flake-compat/1.tar.gz";

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { nixpkgs, home-manager, ... }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {inherit system;config={allowUnfree=true;};}; 
    in {
      homeConfigurations.nagito= home-manager.lib.homeManagerConfiguration {
        inherit pkgs;

        # Specify your home configuration modules here, for example,
        # the path to your home.nix.
        modules = [
	 	  ./users/nagito/home.nix
        ];
        extraSpecialArgs = {
	homeDirectory = "/home/nagito";
	username ="nagito";
	nixpkgs = pkgs;
};
        # Optionally use extraSpecialArgs
        # to pass through arguments to home.nix
      };
    legacyPackages."${system}" = pkgs;
    packages.${system} = pkgs.callPackage packages/package-list.nix {};
    
    };

    }
