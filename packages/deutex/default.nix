{stdenv,fetchFromGitHub,autoconf,automake,pkg-config,libpng}:
  stdenv.mkDerivation {
    version = "5.2.2";
    name = "deutex";
    src = fetchFromGitHub {
        owner = "Doom-Utils";
        repo = "deutex";
        rev = "ef1c06a62cc0eff82ecea984f58fbbe41d8a593d";
        sha256 = "sha256-P7TDOCaUF4Dn9ocXsJOxXkd6MIHm1PSEZJ4iFrI5uFo=";
    
    };   
      preConfigure = "./bootstrap";

      buildInputs = [ autoconf automake pkg-config libpng.dev libpng ];  
}