{fetchurl,lib,stdenvNoCC,p7zip}:
stdenvNoCC.mkDerivation {
 outputs = ["out"  "standardWad" "hcWad" "okWad"];
 dontpatch = true;
 dontbuind = true;
 dontconfigure = true;
 unpackPhase = "7z x $src -y";
 preInstall = "mkdir -p $out/share/games/doom/maps";
 buildPhase = "
 runHook preInstall
 cp ./* -r $out/share/games/doom/maps
 cp $out/share/games/doom/maps/mapsofchaos.wad $standardWad
 cp $out/share/games/doom/maps/mapsofchaos-ok.wad $okWad
 cp $out/share/games/doom/maps/mapsofchaos-hc.wad $hcWad

 ";
 
 name = "maps-of-chaos";
 version = 4.0 ;
 src = fetchurl {
  hash = "sha256-8qBNcJN6MdZc4Ob/Fk+iZKuylmJ6YLmW2ySH/0a8aKQ=";
  urls = [
"https://www.moddb.com/downloads/mirror/63294/130/b466d25f28d09b5f2d375a589f23bb56"
  "https://web.archive.org/web/20240105153507if_/https://sjc1.dl.dbolical.com/dl/2013/12/22/MapsOfChaos.4.zip?st=1MzX2Rb9t0i26-zSoLKOKA==&e=1704472507"

];
 };
buildInputs = [p7zip];
meta = {
license = lib.licenses.gpl3;
homepage = "https://www.moddb.com/mods/brutal-doom/addons/brutalized-doom-and-doom-ii";
};

}
