{fetchurl,stdenvNoCC}:
  stdenvNoCC.mkDerivation rec {
  outputs = ["out" "wad"];
  dontUnpack = true;
  name = "DoomRLArsenal";
  version = "1.1.5";
  installPhase = ''
  cp $src $wad
  mkdir -p $out/share/games/doom/${name}
  cp $src $out/share/games/doom/${name}/${name}_${version}.pk3
  '';
  src = fetchurl {
    urls = [
       "https://mab.greyserv.net/f/DoomRL_Arsenal_1.1.5.pk3"
        "https://web.archive.org/web/20220810073049/https://mab.greyserv.net/f/DoomRL_Arsenal_1.1.5.pk3"
    ];
    hash = "sha256-HFYsYcznw2ouQq92xPGgFmwughEnbVZIFI4HI9KcxTg=";

  };
  }

