{stdenv
  ,python39
  ,fetchFromGitHub
  ,deutex
  ,asciidoc
  ,lib
  ,utils
  ,symlinkJoin
  }:
 let
 freedoom-data = stdenv.mkDerivation (attrs: {
    outputs = ["out" "phase1Wad" "phase2Wad" "freedmWad" ];
    LC_ALL="C.UTF-8";
    version = "${attrs.src.rev}";
    name = "freedoom-data";
    src = fetchFromGitHub {
    owner = "freedoom";
    repo = "freedoom";
    rev = "a66619a60f794a36d1b18aa404b8c02daa081bb2";
    hash = "sha256-Uiifa+bK5lsHRDQcrghmqmTvijXscGYV7PMsw1iXiM8=";

            };
            patchPhase = ''
            patchShebangs scripts graphics/text /build/source/*/*
            substituteInPlace  Makefile --replace "/usr/local" "$out"

            '';
            buildInputs = [(python39.withPackages (p: [p.pillow])) deutex asciidoc] ;
            buildPhase = "
            make  DEUTEX=$deutex wads/freedoom1.wad
            ";
            postInstall = "
            cp wads/freedoom2.wad $phase2Wad
            cp wads/freedoom1.wad $phase1Wad
            cp wads/freedm.wad $freedmWad
            rm -rf $out/bin
            ";
  meta.license = lib.licenses.bsd3;
  });
 p1g = utils.makeLaunchScript {
 name = "freedoom1";
 basewad = freedoom-data.phase1Wad;
 };
 p2g = utils.makeLaunchScript {
  name = "freedoom2";
  basewad = freedoom-data.phase2Wad;

 };
 freedmg = utils.makeLaunchScript {
 name = "freedm";
 basewad = freedoom-data.freedmWad;
 };

 in
symlinkJoin {
 name = "freedoom";
 src = freedoom-data;
 paths = [freedoom-data p1g p2g freedmg ];
 passthru = {
 phase1Wad = freedoom-data.phase1Wad;
 phase2Wad = freedoom-data.phase2Wad;
 freedmWad = freedoom-data.freedmWad;
 };



 meta = freedoom-data.meta;

 }



