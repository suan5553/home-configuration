{gzdoom,writeShellScriptBin,lib,coreutils,...}@fargs:{
  
    makeLaunchScript = {name,basewad ? fargs.freedoom.phase2Wad,extraWads ? [],flags ? [],...}@extraArgs:
    let
    _extraWads = if  lib.isList extraWads then lib.concatStringsSep " " extraWads else "\0";
    _flags = if lib.isList flags then lib.concatStringsSep " " (lib.filter (p: lib.isString p) flags) else "";
    in 
        writeShellScriptBin name
              ''
            
            iwad="$(${coreutils}/bin/mktemp -d)/${name}-basewad.wad"
                ${coreutils}/bin/ln -s "${basewad}" "$iwad" 
              ${gzdoom}/bin/gzdoom -iwad "$iwad" -file  ${_extraWads} ${_flags} "$@"
            rm "$iwad"
            ''; 
        
        
}
