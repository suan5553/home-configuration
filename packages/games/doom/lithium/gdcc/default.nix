{stdenv,
 cmake,
 fetchFromGitHub,
 makeWrapper,
 gmp,
lib}:
stdenv.mkDerivation rec {
  
    name = "GDCC-lithium";
    version = "${src.rev}";
    src = fetchFromGitHub {
          owner = "marrub--";
          repo = "GDCC";
          rev = "926edd7c992279bd5310906623c0c577ec8317f0";  
          hash = "sha256-/kgHXUFC/ItsbLRqZSHCBI7rO2qjPVb81hbuE3rs4BY=";  
    };
    postFixup = ''
    wrapProgram $out/bin/gdcc-makelib  --add-flags "--lib-path $out/share/gdcc/lib"
    wrapProgram $out/bin/gdcc-cc  --add-flags "-I $out/share/gdcc/lib/inc/C"

    '';
    nativeBuildInputs = [cmake gmp makeWrapper ];
    meta.license = lib.licenses.cc0;
}