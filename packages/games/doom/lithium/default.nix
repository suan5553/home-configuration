{gzdoom
, basewad
, flags ? []
, lib
, writeShellApplication
, ninja
, ruby,stdenvNoCC
, fetchFromGitHub
, gdcc ? null
, rambling-trie
, callPackage
, zip
, extraWads ? []
, utils }:
let
GDCC = if gdcc != null then gdcc else callPackage ./gdcc {};
game = utils.makeLaunchScript {
    name = "lithium";
    extraWads = extraWads ++ [wad] ;
    flags = flags;
};
wad = stdenvNoCC.mkDerivation (self: {

    LC_ALL="C.UTF-8";
    version = "${self.src.rev}";
    name = "lithium";
    src = fetchFromGitHub {
     owner = "marrub--";
     repo = "Lithium";
     rev = "4dcec5aa1a8cca3e7a1ca67a1155d167adc5f664";
     hash = "sha256-hayxhvbtOXG5jG7Shj0+5tGgY6HbrKar7h8LaqfU43E=";
    };
    nativeBuildInputs = [ninja ruby  rambling-trie zip GDCC ];
    configurePhase = "ruby ./tools/genbuild.rb";
    preInstall = "zip -r ${self.name}.pk7 pk7/";
    installPhase = "runHook preInstall
    cp ${self.name}.pk7 $out
    ";
    patchPhase = "patchShebangs tools";


});

in
 stdenvNoCC.mkDerivation {
 dontunpack = true;
 outputs = ["out" "wad"];
 buildPhase = "

    mkdir -p  $out/bin $out/share/games/${game.name}
    ln -s ${game}/bin/* $out/bin
    gamedir=$out/share/games/${game.name}
    cp ${wad} $gamedir/${wad.name}
    cp ${wad} $wad
 ";
 src = game;
 name = "lithium";
}


