{utils
, basewad ? null
, RLArsenalPackage ? null
, gdcc
, stdenvNoCC
, fetchFromGitHub
, extraWads ? []
, lib
, flags ? []
,symlinkJoin
}: let
RLA = if  RLArsenalPackage != null then [  RLArsenalPackage doomrpg-rebalanced-data.DRLAPatch] else [];
name = "doomrpg-rebalanced";
game = utils.makeLaunchScript {
inherit name;
inherit flags;
extraWads = extraWads ++ [ doomrpg-rebalanced-data.DoomRPG] ++ RLA  ;
};
doomrpg-rebalanced-data = stdenvNoCC.mkDerivation (attrs: {

  outputs = ["out" "DoomRPG" "DRLAPatch" ];
    LC_ALL="C.UTF-8";
    version = "${attrs.src.rev}";
    name = "doom-rpg-rebalanced";
    src = fetchFromGitHub {
     owner = "WNC12k";
     repo = "DoomRPG-Rebalance";
     rev = "e2e8dcca9d63da49ec3d7d4bab030147daa03eea"; 
     hash = "sha256-qzPZABHNbSCkUwGpeHFd7zmQ4Z917weR9iw4f+QJzRg==";  
    };  
    nativeBuildInputs = [  gdcc  ];
    installPhase = ''
    mkdir -p $out/share/games/doom/${attrs.name}
    cp $src/* -r $out/share/games/doom/${attrs.name}
    mkdir $DRLAPatch
    cp $src/DoomRPG-RLArsenal/* -r $DRLAPatch
    mkdir $DoomRPG
    cp $src/DoomRPG/* -r $DoomRPG
    '';
});
in symlinkJoin {
paths = [doomrpg-rebalanced-data game ];
inherit name;
passthru = {
DoomRPG = doomrpg-rebalanced-data.DoomRPG;
basewad = basewad;
};
}
