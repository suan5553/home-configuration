{callPackage,rambling-trie,deutex,gdcc}: rec{
games = rec {  
  freedoom = callPackage ./freedoom {inherit deutex;inherit utils;};
  lithium = callPackage ./lithium {basewad = freedoom.phase2Wad;inherit rambling-trie;inherit utils;};
  doom-rpg = callPackage ./DoomRPG-rebalanced {gdcc = gdcc;inherit utils;RLArsenalPackage=mods.DoomRLArsenal.wad;basewad=games.freedoom.phase2Wad;};
};
mods = {
  DoomRLArsenal = callPackage ./DoomRLArsenal {};
};
 maps = callPackage ./maps {};
 utils = callPackage ./utils {freedoom=games.freedoom;};
}
