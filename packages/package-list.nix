{callPackage,...}: rec {
 gdcc-lithium = callPackage ./games/doom/lithium/gdcc {};
 rambling-trie = callPackage ./rambling-trie {};
 deutex = callPackage ./deutex {};
 doomPackages = callPackage ./games/doom {inherit rambling-trie;inherit deutex;gdcc=gdcc-lithium;};
 

}

