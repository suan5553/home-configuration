{
  rambling-trie = {
    groups = ["default"];
    platforms = [];
    source = {
      remotes = ["https://rubygems.org"];
      sha256 = "0qg64ls1jgkr4v42s1acq7gsckbhxsm2adalshqc0sss7vnl4xyp";
      type = "gem";
    };
    version = "2.3.1";
  };
}
